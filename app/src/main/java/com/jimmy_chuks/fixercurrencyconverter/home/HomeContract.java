package com.jimmy_chuks.fixercurrencyconverter.home;

import java.util.ArrayList;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

public interface HomeContract {
    interface Presenter{
        void onFetchRate();
        void runDataCheck();
        void setSpinnerItems(ArrayList<String> spinnerItems);
    }

    interface MyView{
        void showTopCurrency();
        void showProgress();
        void hideProgress();
        void initializeSpinners(ArrayList<String> spinnerItems);
        void showChart();
        void showError();
    }
}
