package com.jimmy_chuks.fixercurrencyconverter.home;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jimmy_chuks.fixercurrencyconverter.ApplicationClass;
import com.jimmy_chuks.fixercurrencyconverter.R;
import com.jimmy_chuks.fixercurrencyconverter.models.ApiResponse;
import com.jimmy_chuks.fixercurrencyconverter.retrofit.ApiService;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        HomeContract.MyView{

    @Inject
    ApiService apiService;
    @Inject
    SharedPreferences sharedPreferences;

    private HomePresenter presenter;

    private Spinner firstSpinner;
    private Spinner secondSpinner;
    private ArrayAdapter<CharSequence> firstArrayAdapter;
    private ArrayAdapter<CharSequence> secondArrayAdapter;
    private boolean allSpinnersInitialized;
    private int spinnerInitializerCount;
    private String firstCurrency;
    private String secondCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((ApplicationClass) getApplication()).getComponent().inject(this);
        presenter = new HomePresenter(this, apiService, sharedPreferences);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

        int adapterId = adapterView.getId() ;
        if(allSpinnersInitialized) {
            switch (adapterId) {
                case R.id.first_spinner:
                    firstCurrency = firstSpinner.getSelectedItem().toString();
                    break;
                case R.id.second_spinner:
                    secondCurrency = secondSpinner.getSelectedItem().toString();
                    break;
                default:
                    break;
            }
        }
        if(spinnerInitializerCount < 2){
            spinnerInitializerCount++;
        } else{
            allSpinnersInitialized = true;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void fetchData(){
        Call<ApiResponse> call = apiService.getLatest();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                int responseCode = response.code();
                ApiResponse apiResponse = response.body();
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                ;
            }
        });
    }

    @Override
    public void showTopCurrency() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void initializeSpinners(ArrayList<String> currencyList) {
        firstSpinner = (Spinner) findViewById(R.id.first_spinner);
        firstArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, currencyList);
        firstArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        firstSpinner.setPrompt("Val");
        firstSpinner.setAdapter(firstArrayAdapter);

        secondSpinner = (Spinner) findViewById(R.id.second_spinner);
        secondArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, currencyList);
        secondArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        secondSpinner.setPrompt("Val");
        secondSpinner.setAdapter(secondArrayAdapter);

    }

    @Override
    public void showChart() {

    }

    @Override
    public void showError() {

    }
}
