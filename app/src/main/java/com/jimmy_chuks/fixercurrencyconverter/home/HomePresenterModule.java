package com.jimmy_chuks.fixercurrencyconverter.home;

import com.jimmy_chuks.fixercurrencyconverter.retrofit.ApiService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

@Module
public class HomePresenterModule {
    HomeContract.MyView view;
    ApiService apiService;


    @Provides
    HomeContract.MyView provideSayHelloView() {
        return view;
    }

    @Provides
    ApiService provideApiService() {
        return apiService;
    }
}
