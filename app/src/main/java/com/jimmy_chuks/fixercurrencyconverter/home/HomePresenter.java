package com.jimmy_chuks.fixercurrencyconverter.home;

import android.content.SharedPreferences;

import com.jimmy_chuks.fixercurrencyconverter.retrofit.ApiService;
import com.jimmy_chuks.fixercurrencyconverter.utils.SharedPreferenceManager;

import javax.inject.Inject;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

public class HomePresenter implements HomeContract.Presenter {
    private HomeContract.MyView view;
    private ApiService apiService;
    private SharedPreferenceManager preferenceManager;

    @Inject
    public HomePresenter(HomeContract.MyView view,
                         ApiService apiService, SharedPreferences preferences) {
        this.view = view;
        this.apiService = apiService;
        this.preferenceManager = new SharedPreferenceManager(preferences);
    }

    public void onFetchRate() {

    }

    @Override
    public void runDataCheck() {

    }

}
