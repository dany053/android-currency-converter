package com.jimmy_chuks.fixercurrencyconverter;

import android.app.Application;

import com.jimmy_chuks.fixercurrencyconverter.dagger.AppComponent;
import com.jimmy_chuks.fixercurrencyconverter.dagger.AppModule;
import com.jimmy_chuks.fixercurrencyconverter.dagger.DaggerAppComponent;
import com.jimmy_chuks.fixercurrencyconverter.dagger.NetworkModule;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

public class ApplicationClass extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule("http://api.fixer.io"))
                .build();
    }

    public AppComponent getComponent(){
        return component;
    }
}
