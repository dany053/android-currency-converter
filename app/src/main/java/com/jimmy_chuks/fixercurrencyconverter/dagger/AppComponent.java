package com.jimmy_chuks.fixercurrencyconverter.dagger;

import com.jimmy_chuks.fixercurrencyconverter.home.MainActivity;
import com.jimmy_chuks.fixercurrencyconverter.utils.SharedPreferenceManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

@Singleton
@Component(modules={AppModule.class, NetworkModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
    void inject(SharedPreferenceManager preferenceManager);
}