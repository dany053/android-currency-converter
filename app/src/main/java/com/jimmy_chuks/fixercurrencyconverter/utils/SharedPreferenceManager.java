package com.jimmy_chuks.fixercurrencyconverter.utils;

import android.content.SharedPreferences;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

public class SharedPreferenceManager {


    private SharedPreferences sharedPreferences;

    public SharedPreferenceManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public long getLastUpdateTime(){
        return sharedPreferences.getLong("timeSaved",0l);
    }

    public void setUpdateTime(long currentTime){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("timeSaved",currentTime);
        editor.apply();
    }

    public boolean shouldUpdate(long currentTime){
        long dayLength = 1000 * 60 * 60 * 24;
        long savedTime = sharedPreferences.getLong("timeSaved",0l);
        long timeDifference = currentTime - savedTime;
        if(timeDifference > dayLength){
            return true;
        }
        return false;
    }

    public boolean appDataInitialized(){
        return sharedPreferences.getBoolean("appDataInitialized", false);
    }

    public void setAppDataInitialized(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("appDataInitialized", true);
        editor.apply();
    }
}
