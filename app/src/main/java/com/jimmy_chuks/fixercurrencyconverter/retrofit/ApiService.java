package com.jimmy_chuks.fixercurrencyconverter.retrofit;

import com.jimmy_chuks.fixercurrencyconverter.models.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Jimmy_Chuks on 16/08/2017.
 */

public interface ApiService {

    @GET("/latest")
    Call<ApiResponse> getLatest();

    @GET("/latest")
    Call<ApiResponse> getLatest(@Query("base") String baseCurrency);

    @GET("/{date}")
    Call<ApiResponse> getForDate(@Path("date") String date);

    @GET("/{date}")
    Call<ApiResponse> getForDate(@Path("date") String date, @Query("base") String baseCurrency);
}
